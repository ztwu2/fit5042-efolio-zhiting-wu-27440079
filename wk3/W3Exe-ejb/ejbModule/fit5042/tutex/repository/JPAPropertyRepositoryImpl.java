package fit5042.tutex.repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;

import fit5042.tutex.repository.constants.CommonInstance;
import fit5042.tutex.repository.entities.ContactPerson;
import fit5042.tutex.repository.entities.Property;

@Stateless
public class JPAPropertyRepositoryImpl implements PropertyRepository {
	private ArrayList<Property> propertyList;
	public JPAPropertyRepositoryImpl() {
		propertyList = new ArrayList<Property>();
		initialisePropertyList();
	}
	
	public void initialisePropertyList(){
		propertyList.clear();
		propertyList.add(CommonInstance.PROPERTY_FIRST);
		propertyList.add(CommonInstance.PROPERTY_SECOND);
		propertyList.add(CommonInstance.PROPERTY_THIRD);
		propertyList.add(CommonInstance.PROPERTY_FOURTH);
	}
	
	public List<ContactPerson> getAllContactPeople(){
		//use hashset to make the contact person not duplicated
		HashSet<ContactPerson> contactPeople1 = new HashSet<ContactPerson>();
		for(Property p: propertyList) {
			contactPeople1.add(p.getContactPerson());
		}
		List<ContactPerson> contactPeople = new ArrayList<ContactPerson>();
		for(ContactPerson c: contactPeople1) {
			contactPeople.add(c);
		}
		return contactPeople;
	}
	//why must I use Set<Property> rather than Property
	/*
	public Property searchPropertyByContactPerson(ContactPerson contactPerson){
		Property p = new Property();
		for(Property q: propertyList){
			if(q.getContactPerson().equals(contactPerson)){
				p = q;
			}
		}
		return p;
	}
	*/
	public Set<Property> searchPropertyByContactPerson(ContactPerson contactPerson){
		Set<Property> pro = new HashSet<>();
		for(Property p: propertyList) {
			if(p.getContactPerson().equals(contactPerson)) {
				pro.add(p);
			}
		}
		return pro;
	}
	
	public List<Property> searchPropertyByBudget(double budget){
		List<Property> specificProperty = new ArrayList<Property>();
		for(Property p: propertyList) {
			if(p.getPrice() <= budget) {
				specificProperty.add(p);
			}
		}
		return specificProperty;
	}
	public ArrayList<Property> getPropertyList() {
		return propertyList;
	}

	public void setPropertyList(ArrayList<Property> propertyList) {
		this.propertyList = propertyList;
	}
	
	public void removeProperty(int propertyId) {
    	for (Property p : propertyList) {
    		if (p.getPropertyId() == propertyId) {
    			propertyList.remove(p);
    			break;
    		}
    	}
    	
    }
    
    public void addProperty(Property property) {
    	propertyList.add(property);
    }
    
    public void editProperty(Property property) {
    	for (Property p : propertyList) {
    		int id = property.getPropertyId();
    		if (p.getPropertyId() == id) {
    			propertyList.set(id, property);
    			break;
    		}
    	}
    }

	public int getPropertyId() {
		return propertyList.get(propertyList.size() - 1).getPropertyId();
	}
	
	public Property searchPropertyById(int propertyId) {
		for (Property p : propertyList) {
    		if (p.getPropertyId() == propertyId) {
    			return p;
    		}
    	}
		return null;
	}
	
}
