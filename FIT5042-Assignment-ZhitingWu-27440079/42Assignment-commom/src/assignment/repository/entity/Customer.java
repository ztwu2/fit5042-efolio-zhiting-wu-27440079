package assignment.repository.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CUSTOMER")
@NamedQueries({@NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT c FROM Customer c order by c.customerId desc")})
public class Customer implements Serializable {
    
    public static final String GET_ALL_QUERY_NAME = "Customer.getAll";
//    public static final String GET_INDEX = "Customer.getMax";
    
    private int customerId;
    private String customerName;
	private String industryType;
	private String lastPurchase;
	private Double totalPurchase;
	private String country;
    private Address address;
    
    private Set<Contact> contacts;

    public Customer() {
    }

    public Customer(int conactPersonId, String name, String industryType, String lastPurchase, Double totalPurchase, String country, Address address) {
        this.customerId = conactPersonId;
        this.customerName = name;
        this.industryType = industryType;
        this.lastPurchase = lastPurchase;
        this.totalPurchase = totalPurchase;
        this.country = country;
        this.address = address;
        this.contacts = new HashSet<>();
    }

    @Id
    @GeneratedValue
    @Column(name = "customer_id")
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int conactPersonId) {
        this.customerId = conactPersonId;
    }

    @Column(name = "customer_name")
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String name) {
        this.customerName = name;
    }

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public Set<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }
    
    @Column(name = "industry_type")
	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	@Column(name = "last_purchase")
	public String getLastPurchase() {
		return lastPurchase;
	}

	public void setLastPurchase(String lastPurchase) {
		this.lastPurchase = lastPurchase;
	}

	@Column(name = "total_purchase")
	public Double getTotalPurchase() {
		return totalPurchase;
	}

	public void setTotalPurchase(Double totalPurchase) {
		this.totalPurchase = totalPurchase;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
    @Embedded
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.customerId;
        return hash;
    }
    
    @Override
    public String toString() {
        return this.customerId + "customerId - " + customerId + " customerName - " + customerName + 
        		" industryType - " + industryType + " lastPurchase - " + lastPurchase +
        		" totalPurchase - " + totalPurchase + " country - " + country +
        		" address - " + address;
    }
}
