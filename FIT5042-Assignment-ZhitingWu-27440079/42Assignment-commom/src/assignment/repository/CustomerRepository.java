package assignment.repository;

import java.util.List;

import javax.ejb.Remote;

import assignment.repository.entity.Contact;
import assignment.repository.entity.Customer;

@Remote
public interface CustomerRepository {

    public void addCustomer(Customer cus) throws Exception;
    
    public Customer searchCustomerById(int id) throws Exception;
    
    public List<Contact> getAllContacts() throws Exception;
    
    public List<Customer> getAllCustomers() throws Exception;
    
    public void removeCustomer(int customerId) throws Exception;
    
    public void editCustomer(Customer customer) throws Exception;

}
