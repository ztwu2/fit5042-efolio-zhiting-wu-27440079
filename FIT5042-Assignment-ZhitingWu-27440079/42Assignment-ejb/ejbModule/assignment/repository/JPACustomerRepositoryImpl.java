package assignment.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import assignment.repository.entity.Contact;
import assignment.repository.entity.Customer;

@Stateless
public class JPACustomerRepositoryImpl implements CustomerRepository {

	@PersistenceContext(unitName = "42Assignment-ejbPU")
	private EntityManager entityManager;
	
	@Override
	public void addCustomer(Customer customer) throws Exception {
		List<Customer> customers =  entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList(); 
		customer.setCustomerId(customers.get(0).getCustomerId() + 1);
        entityManager.persist(customer);
	}
	
	@Override
	public Customer searchCustomerById(int id) throws Exception {
		Customer customer = entityManager.find(Customer.class, id);
		return customer;
	}
	
	@Override
	public List<Customer> getAllCustomers() throws Exception {
		return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
	}
	
	@Override
	public List<Contact> getAllContacts() throws Exception {
		return entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList();
	}
	
	@Override
	public void removeCustomer(int id) throws Exception {
		Customer customer = this.searchCustomerById(id);
		
		if (customer != null) {
			entityManager.remove(customer);
		}
	}
	
	@Override
	public void editCustomer(Customer customer) throws Exception {
		try {
			entityManager.merge(customer);
		} catch (Exception e) {
			
		}
	}
	
}
