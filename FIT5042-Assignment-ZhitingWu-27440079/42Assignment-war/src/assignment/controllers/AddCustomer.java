package assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import assignment.mbeans.CustomerManagedBean;
import assignment.repository.entity.Contact;

import javax.faces.bean.*;

@RequestScoped
@Named("addCustomer")
public class AddCustomer {

	@ManagedProperty(value="#{customerManagedBean}")
	CustomerManagedBean customerManagedBean;
	
	private boolean showForm = true;
	
	private Customer customer;
	
	CustomerApplication app;
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public boolean isShowForm() {
		return showForm;
	}
	
	public AddCustomer() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		
		app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");
		
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
		        .getELResolver().getValue(elContext, null, "customerManagedBean");
		
	}
	
	public void addCustomer(Customer customer) {
		try {
			customerManagedBean.addCustomer(customer);
			app.searchAll();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been added succesfully"));
            
		} catch (Exception e) {
			
		}
		showForm = true;
	}
	
	
}
