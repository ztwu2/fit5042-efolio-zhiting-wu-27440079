package assignment.controllers;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

@Named(value = "titleController")
@RequestScoped
public class TitleController {
	
    private String pageTitle;

    public TitleController() {
        pageTitle = "AUSPrintings Pty Ltd";
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
    
}
