package assignment.controllers;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.Column;

import assignment.repository.entity.Address;
import assignment.repository.entity.Contact;

@RequestScoped
@Named(value = "customer")
public class Customer implements Serializable {

	private int customerId;
	private String customerName;
	private String industryType;
	private String lastPurchase;
	
	private Address address;
	
    private String streetNumber;
    private String streetAddress;
    private String suburb;
    private String postcode;
    private String state;
    
	private Double totalPurchase;
	private String country;
	private HashSet<Contact> contacts;
	
	private Set<assignment.repository.entity.Customer> customers;
	
	public Customer() {
		this.contacts = new HashSet<>();
	}
	
	public Customer(int customerId, String customerName, String industryType, String lastPurchase, Address address, Double totalPurchase, String country, Set<Contact> contacts) {
		this.customerId = customerId;
		this.customerName = customerName;
		this.industryType = industryType;
		this.lastPurchase = lastPurchase;
		this.address = address;
		this.totalPurchase = totalPurchase;
		this.country = country;
		this.contacts = (HashSet<Contact>) contacts;
	}
	
    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetAddress() {
        return streetAddress;
    }
    

    public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    public Set<assignment.repository.entity.Customer> getCustomers() {
    	return customers;
    }
    
    public void setCustomers(Set<assignment.repository.entity.Customer> customers) {
    	this.customers = customers;
    }
    
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String name) {
        this.customerName = name;
    }
    
    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }
    
    @Column(name = "last_purchase")
    public String getLastPurchase() {
        return lastPurchase;
    }

    public void setLastPurchase(String lastPurchase) {
        this.lastPurchase = lastPurchase;
    }
    
    
    public Double getTotalPurchase() {
        return totalPurchase;
    }

    public void setTotalPurchase(Double totalPurchase) {
        this.totalPurchase = totalPurchase;
    }
    
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    public HashSet<Contact> getContacts() {
    	return contacts;
    }
    
    public void setContacts(HashSet<Contact> contacts) {
    	this.contacts = contacts;
    }	
	
}
