package assignment.controllers;

import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import assignment.mbeans.CustomerManagedBean;
import assignment.repository.entity.Customer;

import javax.faces.bean.ManagedProperty;

@Named(value = "customerApplication")
@ApplicationScoped

public class CustomerApplication {

	@ManagedProperty(value="{customerManagedBean}")
	CustomerManagedBean customerManagedBean;
	
	private ArrayList<Customer> customers;

	
	public CustomerApplication() throws Exception {
		customers = new ArrayList<>();
		
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(elContext, null, "customerManagedBean");
	
		updateCustomerList();
	}
	
	
	public ArrayList<Customer> getCustomers(){
		return customers;
	}
	
    private void setCustomers(ArrayList<Customer> c) {
        this.customers = c;
    }
    
    public void updateCustomerList() {
    	if (customers != null && customers.size() > 0) {
    		
    	} else {
    		customers.clear();
    		
    		for (assignment.repository.entity.Customer customer : customerManagedBean.getAllCustomers()) 
    		{
    			customers.add(customer);
    		}
    		
    		setCustomers(customers);
    	}
    }
	
    public void searchCustomerById(int id) {
    	customers.clear();
    	customers.add(customerManagedBean.searchCustomerById(id));
    }
	
    public void searchAll() {
    	customers.clear();
    	
    	for (assignment.repository.entity.Customer customer : customerManagedBean.getAllCustomers()) {
			customers.add(customer);
		}
    	setCustomers(customers);
    }
	
}
