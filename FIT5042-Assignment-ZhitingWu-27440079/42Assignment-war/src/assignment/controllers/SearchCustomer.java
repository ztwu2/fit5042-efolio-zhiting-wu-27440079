package assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;


@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
	
	private Customer customer;
	
	CustomerApplication app;
	
	private int searchByInt;
	
	public CustomerApplication getApp() {
		return app;
	}
	
    public void setApp(CustomerApplication app) {
        this.app = app;
    }

    public void setCustomer(Customer customer) {
    	this.customer = customer;
    	
    }
    
    public Customer getCustomer() {
    	return customer;
    }
    
    public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }
    
    public SearchCustomer() {
    	ELContext context
        = FacesContext.getCurrentInstance().getELContext();

    	app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");

	app.updateCustomerList();
	}
    
    public void searchCustomerById(int id) {
    	try {
    		app.searchCustomerById(id);
    	} catch (Exception e) {
    		
    	}
    }
    
    public void searchAll() {
    	try {
    		app.searchAll();
    	} catch (Exception e) {
    		
    	}
    }
    
}
