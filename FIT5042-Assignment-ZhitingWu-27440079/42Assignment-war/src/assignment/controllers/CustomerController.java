package assignment.controllers;

import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

@Named(value = "customerController")
@RequestScoped

public class CustomerController {

	private int customerId;
	
	public int getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(int id) {
		this.customerId = id;
	}
	
	private assignment.repository.entity.Customer customer;
	
	public CustomerController() {
		customerId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerID"));
		customer = getCustomer();
	}
	
	public assignment.repository.entity.Customer getCustomer(){
		if (customer == null) {
			ELContext context
            = FacesContext.getCurrentInstance().getELContext();
			
			CustomerApplication app = (CustomerApplication) FacesContext.getCurrentInstance()
                    .getApplication()
                    .getELResolver()
                    .getValue(context, null, "customerApplication");
			
			return app.getCustomers().get(--customerId); //in index
		
		
		}
		
		return customer;
	}
}
