package assignment.mbeans;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.faces.bean.ManagedBean;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import assignment.repository.CustomerRepository;
import assignment.repository.entity.Address;
import assignment.repository.entity.Customer;

@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable {

	@EJB
	CustomerRepository customerRepository;
	
	public CustomerManagedBean() {

	}
	
	public List<Customer> getAllCustomers(){
		try {
			List<Customer> customers = customerRepository.getAllCustomers();
			return customers;
		} catch (Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
	
	public void addCustomer(Customer cus) {
		try {
			customerRepository.addCustomer(cus);
		} catch (Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}       
	}

	public Customer searchCustomerById(int id) {
		try {
			return customerRepository.searchCustomerById(id);
		} catch (Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
		
		return null;
	}
	
	public void removeCustomer(int id) {
		try {
			customerRepository.removeCustomer(id);
		} catch (Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
	public void editCustomer(Customer customer) {
		try {
			String s = customer.getAddress().getStreetNumber();
			Address address = customer.getAddress();
			address.setStreetNumber(s);
			customer.setAddress(address);
			
			customerRepository.editCustomer(customer);
			        
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been updated succesfully"));
        } catch (Exception e) {
        	Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
        }
	}
	
	public void addCustomer(assignment.controllers.Customer cus) {
		Customer customer = covertCustomerToEntity(cus);
		try {
			customerRepository.addCustomer(customer);
		} catch (Exception e) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, e);
		}
	}
	
	public Customer covertCustomerToEntity(assignment.controllers.Customer localCustomer) {
		Customer customer = new Customer();
		String streetNumber = localCustomer.getStreetNumber();
		String streetAddress = localCustomer.getStreetAddress();
        String suburb = localCustomer.getSuburb();
        String postcode = localCustomer.getPostcode();
        String state = localCustomer.getState();
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
        customer.setAddress(address);
        customer.setCustomerName(localCustomer.getCustomerName());
        customer.setCountry(localCustomer.getCountry());
        customer.setIndustryType(localCustomer.getIndustryType());
        customer.setLastPurchase(localCustomer.getLastPurchase());
        customer.setTotalPurchase(localCustomer.getTotalPurchase());
        
		
		return customer;
	}
}
