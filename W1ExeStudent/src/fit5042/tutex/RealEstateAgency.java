package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;
    ArrayList<Property> propertyList = new ArrayList<Property>();
    

//	public RealEstateAgency(String name, PropertyRepository propertyRepository, ArrayList<Property> propertyList) {
//		super();
//		this.name = name;
//		this.propertyRepository = propertyRepository;
//		this.propertyList = propertyList;
//	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
    	Property a = new Property(1, "24 Boston Ave, Malvern East VIC 3145, Australia", 2, 150, 420020);
    	Property b = new Property(2, "212 Boston Ave, Malvern East VIC 3145, Australia", 2, 150, 4223400);
    	Property c = new Property(3, "2234 Boston Ave, Malvern East VIC 3145, Australia", 2, 150, 454000);
    	Property d = new Property(4, "241 Boston Ave, Malvern East VIC 3145, Australia", 2, 150, 4600300);
    	Property e = new Property(5, "224 Boston Ave, Malvern East VIC 3145, Australia", 2, 150, 430230);
    	
        propertyList.add(a);
        propertyList.add(b);
        propertyList.add(c);
        propertyList.add(d);
        propertyList.add(e);
        
        System.out.println("5 properties added successfully");
        
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
        for(int i=0;i<propertyList.size();i++)  
        {  
         System.out.println(propertyList.get(i));     
        }  
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
    	Scanner kb = new Scanner(System.in);

        int num1;
        System.out.print("Enter the ID of the property you want to search: ");
                num1 = Integer.parseInt(kb.nextLine());
                
                int searchListLength = propertyList.size();
                for (int i = 0; i < searchListLength; i++) {
                if (propertyList.get(i).getId() == num1) {
                	System.out.println(propertyList.get(i));
                }
                }
    }
    
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
//        	run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }

	public PropertyRepository getPropertyRepository() {
		return propertyRepository;
	}
}
