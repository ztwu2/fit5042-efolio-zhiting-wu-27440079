/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */

public abstract class SimplePropertyRepositoryImpl implements PropertyRepository{
	ArrayList<Property> a;

    public SimplePropertyRepositoryImpl(ArrayList<Property> a) {
		super();
		this.a = a;
	}

	public ArrayList<Property> getA() {
		return a;
	}

	public void setA(ArrayList<Property> a) {
		this.a = a;
	}

	public SimplePropertyRepositoryImpl() {
        
    }
    
}
